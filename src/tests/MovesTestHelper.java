package tests;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import model.Peca;

public class MovesTestHelper {
	
	public static final int POSITION_X = 4;
	public static final int POSITION_Y = 4;
	
	public static ArrayList<Point> getTowermovimenta() {
		int x = POSITION_X;
		int y = POSITION_Y;
		ArrayList<Point> movimenta = new ArrayList<Point>();

		movimenta.add(new Point(x - 4, y));
		movimenta.add(new Point(x - 3, y));
		movimenta.add(new Point(x - 2, y));
		movimenta.add(new Point(x - 1, y));
		movimenta.add(new Point(x + 1, y));
		movimenta.add(new Point(x + 2, y));
		movimenta.add(new Point(x + 3, y));
		movimenta.add(new Point(x, y - 4));
		movimenta.add(new Point(x, y - 3));
		movimenta.add(new Point(x, y - 2));
		movimenta.add(new Point(x, y - 1));
		movimenta.add(new Point(x, y + 1));
		movimenta.add(new Point(x, y + 2));
		movimenta.add(new Point(x, y + 3));

		return movimenta;
	}
	
	public static ArrayList<Point> getHorsemovimenta() {
		int x = POSITION_X;
		int y = POSITION_Y;
		ArrayList<Point> movimenta = new ArrayList<Point>();

		movimenta.add(new Point(x - 1, y + 2));
		movimenta.add(new Point(x + 1, y + 2));
		movimenta.add(new Point(x + 1, y - 2));
		movimenta.add(new Point(x - 1, y - 2));
		movimenta.add(new Point(x - 2, y + 1));
		movimenta.add(new Point(x - 2, y - 1));
		movimenta.add(new Point(x + 2, y + 1));
		movimenta.add(new Point(x + 2, y - 1));

		return movimenta;
	}
	
	public static ArrayList<Point> getBishopmovimenta() {
		int x = POSITION_X;
		int y = POSITION_Y;
		ArrayList<Point> movimenta = new ArrayList<Point>();

		movimenta.add(new Point(x - 1, y + 1));
		movimenta.add(new Point(x - 2, y + 2));
		movimenta.add(new Point(x - 3, y + 3));
		movimenta.add(new Point(x - 1, y - 1));
		movimenta.add(new Point(x - 2, y - 2));
		movimenta.add(new Point(x - 3, y - 3));
		movimenta.add(new Point(x - 4, y - 4));
		movimenta.add(new Point(x + 1, y + 1));
		movimenta.add(new Point(x + 2, y + 2));
		movimenta.add(new Point(x + 3, y + 3));
		movimenta.add(new Point(x + 1, y - 1));
		movimenta.add(new Point(x + 2, y - 2));
		movimenta.add(new Point(x + 3, y - 3));

		return movimenta;
	}

	public static ArrayList<Point> getKingmovimenta() {
		int x = POSITION_X;
		int y = POSITION_Y;
		ArrayList<Point> movimenta = new ArrayList<Point>();

		movimenta.add(new Point(x + 1, y + 1));
		movimenta.add(new Point(x + 0, y + 1));
		movimenta.add(new Point(x - 1, y + 1));
		movimenta.add(new Point(x - 1, y + 0));
		movimenta.add(new Point(x - 1, y - 1));
		movimenta.add(new Point(x + 0, y - 1));
		movimenta.add(new Point(x + 1, y - 1));
		movimenta.add(new Point(x + 1, y + 0));

		return movimenta;
	} 
	
	public static ArrayList<Point> getQueenmovimenta(){
		
		int x = POSITION_X;
		int y = POSITION_Y;
		ArrayList<Point> movimenta = new ArrayList<Point>();

		movimenta.add(new Point(x - 4, y));
		movimenta.add(new Point(x - 3, y));
		movimenta.add(new Point(x - 2, y));
		movimenta.add(new Point(x - 1, y));
		movimenta.add(new Point(x + 1, y));
		movimenta.add(new Point(x + 2, y));
		movimenta.add(new Point(x + 3, y));
		movimenta.add(new Point(x, y - 4));
		movimenta.add(new Point(x, y - 3));
		movimenta.add(new Point(x, y - 2));
		movimenta.add(new Point(x, y - 1));
		movimenta.add(new Point(x, y + 1));
		movimenta.add(new Point(x, y + 2));
		movimenta.add(new Point(x, y + 3));
		movimenta.add(new Point(x - 1, y + 1));
		movimenta.add(new Point(x - 2, y + 2));
		movimenta.add(new Point(x - 3, y + 3));
		movimenta.add(new Point(x - 1, y - 1));
		movimenta.add(new Point(x - 2, y - 2));
		movimenta.add(new Point(x - 3, y - 3));
		movimenta.add(new Point(x - 4, y - 4));
		movimenta.add(new Point(x + 1, y + 1));
		movimenta.add(new Point(x + 2, y + 2));
		movimenta.add(new Point(x + 3, y + 3));
		movimenta.add(new Point(x + 1, y - 1));
		movimenta.add(new Point(x + 2, y - 2));
		movimenta.add(new Point(x + 3, y - 3));

		return movimenta;
		
	}
	public static ArrayList<Point> getPeaomovimenta(Peca peao){
		
		int x = POSITION_X;
		int y = POSITION_Y;
		ArrayList<Point> movimenta = new ArrayList<Point>();
		
		if(peao.getTeam()== Color.WHITE)
			movimenta.add(new Point(x-1,y));
		else
			movimenta.add(new Point(x+1, y));

		return movimenta;	
	}
}