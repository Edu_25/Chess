package tests;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;


import model.Bispo;
import model.Cavalo;
import model.Peao;
import model.Peca;
import model.Rainha;
import model.Rei;
import model.Square;
import model.Torre;

import org.junit.Assert;
import org.junit.Test;

import control.SquareControl;

public class PieceTest{

	@Test
	public void testMoveTorre() throws Exception {
		SquareControl control = new SquareControl();//declara squareControl
		Peca torre = new Torre("Torre","icon/White R_48x48.png",Color.WHITE);//Cria uma peça
		//Adiciona a peça no quadrado da posição do teste
		control.getSquare(MovesTestHelper.POSITION_X,MovesTestHelper.POSITION_Y).setPeca(torre);//Dica pro roque
		control.setSelecionaCasa(control.getSquare(MovesTestHelper.POSITION_X,MovesTestHelper.POSITION_Y));//adiciona o quadrado selecionado exigido pelo método movimenta
		
		torre.movimentaPeca(control, Color.BLACK);//Marca os quadrados com a cor Orage que podem ser movidos
		
		ArrayList<Point> listaPontos = new ArrayList<Point>();
		//adiciona pontos em array: pontos que podem mover
		for(Square square : control.getSquareList()){
			if(square.getColor() == Color.ORANGE){
				listaPontos.add(square.getPosition());
			}
		}
		//Foi removido o quadrado onde está a peça!
		assertMoves(MovesTestHelper.getTowermovimenta(), listaPontos);//testa movimentos
	}
	
	@Test
	public void testMoveCavalo() throws Exception{
		SquareControl control = new SquareControl();
		Peca cavalo = new Cavalo("Cavalo","icon/White N_48x48.png",Color.WHITE);
		control.getSquare(MovesTestHelper.POSITION_X,MovesTestHelper.POSITION_Y).setPeca(cavalo);
		control.setSelecionaCasa(control.getSquare(MovesTestHelper.POSITION_X,MovesTestHelper.POSITION_Y));
		
		cavalo.movimentaPeca(control, Color.BLACK);
		
		ArrayList<Point> listaPontos = new ArrayList<Point>();
		for(Square square : control.getSquareList()){
			if(square.getColor() == Color.ORANGE){
				listaPontos.add(square.getPosition());
			}
		}
		assertMoves(MovesTestHelper.getHorsemovimenta(), listaPontos);//testa movimentos	
	}
	
	@Test
	public void testMoveBispo() throws Exception{
		SquareControl control = new SquareControl();//declara squareControl
		Peca bispo = new Bispo("Bispo","icon/White B_48x48.png",Color.WHITE);//Cria uma peça
		control.getSquare(MovesTestHelper.POSITION_X,MovesTestHelper.POSITION_Y).setPeca(bispo);//Dica pro roque
		control.setSelecionaCasa(control.getSquare(MovesTestHelper.POSITION_X,MovesTestHelper.POSITION_Y));
		bispo.movimentaPeca(control, Color.BLACK);
		ArrayList<Point> listaPontos = new ArrayList<Point>();
		//adiciona pontos em array: pontos que podem mover
		for(Square square : control.getSquareList()){
			if(square.getColor() == Color.ORANGE){
				listaPontos.add(square.getPosition());
			}
		}
		assertMoves(MovesTestHelper.getBishopmovimenta(), listaPontos);
	}

	@Test
	public void testMoveRainha() throws Exception{
		SquareControl control = new SquareControl();
		Peca rainha = new Rainha("Rainha","icon/White Q_48x48.png",Color.WHITE);
		control.getSquare(MovesTestHelper.POSITION_X,MovesTestHelper.POSITION_Y).setPeca(rainha);
		control.setSelecionaCasa(control.getSquare(MovesTestHelper.POSITION_X,MovesTestHelper.POSITION_Y));
		
		rainha.movimentaPeca(control, Color.BLACK);
		
		ArrayList<Point> listaPontos = new ArrayList<Point>();

		for(Square square : control.getSquareList()){
			if(square.getColor() == Color.ORANGE){
				listaPontos.add(square.getPosition());
			}
		}
		assertMoves(MovesTestHelper.getQueenmovimenta(), listaPontos);
	}
	
	
	@Test
	public void testMoveRei() throws Exception{
		SquareControl control = new SquareControl();
		Peca rei = new Rei("Rei","icon/White K_48x48.png",Color.WHITE);
		control.getSquare(MovesTestHelper.POSITION_X,MovesTestHelper.POSITION_Y).setPeca(rei);
		control.setSelecionaCasa(control.getSquare(MovesTestHelper.POSITION_X,MovesTestHelper.POSITION_Y));
		
		rei.movimentaPeca(control, Color.BLACK);
		
		ArrayList<Point> listaPontos = new ArrayList<Point>();
		for(Square square : control.getSquareList()){
			if(square.getColor() == Color.ORANGE){
				listaPontos.add(square.getPosition());
			}
		}
		assertMoves(MovesTestHelper.getKingmovimenta(), listaPontos);
		}

	@Test
	public void testMovePeao() throws Exception{
		SquareControl control = new SquareControl();
		Peca peao_claro = new Peao("Peao","icon/White P_48x48.png",Color.WHITE);
		
		control.getSquare(MovesTestHelper.POSITION_X,MovesTestHelper.POSITION_Y).setPeca(peao_claro); 
		control.setSelecionaCasa(control.getSquare(MovesTestHelper.POSITION_X,MovesTestHelper.POSITION_Y));
		peao_claro.movimentaPeca(control, Color.WHITE);
	
		ArrayList<Point> listaPontos = new ArrayList<Point>();
		for(Square square : control.getSquareList()){
			if(square.getColor() == Color.ORANGE){
				listaPontos.add(square.getPosition());
			}
		}
		Peca peao_escuro = new Peao("Peao","icon/Black P_48x48.png",Color.BLACK);
		control.getSquare(MovesTestHelper.POSITION_X,MovesTestHelper.POSITION_Y).setPeca(peao_escuro); 
		control.setSelecionaCasa(control.getSquare(MovesTestHelper.POSITION_X,MovesTestHelper.POSITION_Y));
		peao_claro.movimentaPeca(control, Color.BLACK);
	
		ArrayList<Point> listaPontos2 = new ArrayList<Point>();
		for(Square square : control.getSquareList()){
			if(square.getColor() == Color.ORANGE){
				listaPontos2.add(square.getPosition());
			}
		}
		
	}

	private void assertMoves(ArrayList<Point> movesA, ArrayList<Point> movesB) throws Exception {
		if (movesA.size() != movesB.size()) {//Compara o tamanho das duas listas
			Assert.assertTrue(false);//Teste deu errado
			return;//encerrar a função
		}

		for (Point point : movesA) {//Percorre a lista elemento por elemento
			if (!movesB.contains(point)) {//Compara se os elementos da lista MovesTestHelper estão na lista gerada por você
				Assert.assertTrue(false);//se não tiver Teste deu errado
				return;//encerra função
			}
		}
		Assert.assertTrue(true);//Se a função não foi encerrada Teste Deu certo!
	}
}