package model;
import java.awt.Color;
import java.awt.Point;

import javax.swing.JOptionPane;

import control.SquareControl;
/*COMENTARIO A RESPEITO DA MOVIMENTAÇÃO:
 * A movimentação do cavalo se baseia em L's
 * Então foi necessario pintar as casas que estão que se distanciam uma na horizontal e duas na vertical ou vice-versa 
 * partindo-se das coordenadas da torre no momento, Para correçao de alguns erros foi necessario colocar certas contençoes ,uma vez que,
 * sem elas extrapolava-se o tabuleiro ou seja gerava erros na movimentação;
*/
public class Cavalo extends Peca{
	public Cavalo(String name, String posicao, Color team){
		super(name,posicao,team);
	}

    @Override
    public void movimentaPeca(SquareControl control,Color team){
    
		Point coordenadas = control.getSelecionaCasa().getPosition();
		//Pinta as casas possiveis a esquerda abaixo do cavalo com exceção 
			if(coordenadas.y > 1 && coordenadas.x > 0){
				if(control.getSquare(coordenadas.x-1,coordenadas.y-2).getPeca()==null)
					control.getSquare(coordenadas.x-1,coordenadas.y-2).setColor(Color.ORANGE);
				else if(control.getSquare(coordenadas.x-1,coordenadas.y-2).getPeca().getTeam()!=team){
					control.getSquare(coordenadas.x-1,coordenadas.y-2).setColor(Color.RED);
					if(control.getSquare(coordenadas.x-1,coordenadas.y-2).getPeca().getName()=="Rei"){
						if(control.getSquare(coordenadas.x-1,coordenadas.y-2).getPeca().getTeam()==Color.WHITE)  
							JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
						else
							JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
					}
				}
			}
			//Pinta as casas possiveis a direita acima do cavalo com exceção
			if(coordenadas.y < 6 && coordenadas.x < 7){
				if(control.getSquare(coordenadas.x+1,coordenadas.y+2).getPeca()==null)
					control.getSquare(coordenadas.x+1,coordenadas.y+2).setColor(Color.ORANGE);
				else if(control.getSquare(coordenadas.x+1,coordenadas.y+2).getPeca().getTeam()!= team){
					control.getSquare(coordenadas.x+1,coordenadas.y+2).setColor(Color.RED);
					if(control.getSquare(coordenadas.x+1,coordenadas.y+2).getPeca().getName()=="Rei"){
						if(control.getSquare(coordenadas.x+1,coordenadas.y+2).getPeca().getTeam()==Color.WHITE)  
							JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
						else
							JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
					}
				}
    		}
			//Pinta as casas possiveis a esquerda acima do cavalo com exceção
			if(coordenadas.y > 1 && coordenadas.x < 7){
				if(control.getSquare(coordenadas.x+1,coordenadas.y-2).getPeca()==null)
					control.getSquare(coordenadas.x+1,coordenadas.y-2).setColor(Color.ORANGE);
				else if(control.getSquare(coordenadas.x+1,coordenadas.y-2).getPeca().getTeam()!= team){
					control.getSquare(coordenadas.x+1,coordenadas.y-2).setColor(Color.RED);
					if(control.getSquare(coordenadas.x+1,coordenadas.y-2).getPeca().getName()=="Rei"){
						if(control.getSquare(coordenadas.x+1,coordenadas.y-2).getPeca().getTeam()==Color.WHITE)  
							JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
						else
							JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
						
					}
				}
    		}
			//Pinta as casas possiveis a direita abaixo do cavalo com exceção
			if(coordenadas.x > 1 && coordenadas.y > 0){
				if(control.getSquare(coordenadas.x-2,coordenadas.y-1).getPeca()==null)
					control.getSquare(coordenadas.x-2,coordenadas.y-1).setColor(Color.ORANGE);
				else if(control.getSquare(coordenadas.x-2,coordenadas.y-1).getPeca().getTeam()!=team){
					control.getSquare(coordenadas.x-2,coordenadas.y-1).setColor(Color.RED);
					if(control.getSquare(coordenadas.x-2,coordenadas.y-1).getPeca().getName()=="Rei"){
						if(control.getSquare(coordenadas.x-2,coordenadas.y-1).getPeca().getTeam()==Color.WHITE)  
							JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
						else
							JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
					}
				}
			}
			//Pinta as casas possiveis a direita abaixo abaixo do cavalo com exceção
			if(coordenadas.x > 1 && coordenadas.y < 7){
				if(control.getSquare(coordenadas.x-2,coordenadas.y+1).getPeca()==null)
					control.getSquare(coordenadas.x-2,coordenadas.y+1).setColor(Color.ORANGE);
				else if( control.getSquare(coordenadas.x-2,coordenadas.y+1).getPeca().getTeam()!=team){
					control.getSquare(coordenadas.x-2,coordenadas.y+1).setColor(Color.RED);	
					if(control.getSquare(coordenadas.x-2,coordenadas.y+1).getPeca().getName()=="Rei"){
						if(control.getSquare(coordenadas.x-2,coordenadas.y+1).getPeca().getTeam()==Color.WHITE)  
							JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
						else
							JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
					}
				}
			}
			//Pinta as casas possiveis a esquerda acima acima do cavalo com exceção
			if(coordenadas.y < 6 && coordenadas.x > 0){
				if(control.getSquare(coordenadas.x-1,coordenadas.y+2).getPeca()==null)
					control.getSquare(coordenadas.x-1,coordenadas.y+2).setColor(Color.ORANGE);
				else if( control.getSquare(coordenadas.x-1,coordenadas.y+2).getPeca().getTeam()!=team){
					control.getSquare(coordenadas.x-1,coordenadas.y+2).setColor(Color.RED);	
					if(control.getSquare(coordenadas.x-1,coordenadas.y+2).getPeca().getName()=="Rei"){
						if(control.getSquare(coordenadas.x-1,coordenadas.y+2).getPeca().getTeam()==Color.WHITE)  
							JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
						else
							JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
					}
				}
			}
			//Pinta as casas possiveis a esquerda abaixo abaixo do cavalo com exceção
			if(coordenadas.x < 6 && coordenadas.y > 0){
				if(control.getSquare(coordenadas.x+2,coordenadas.y-1).getPeca()==null )
					control.getSquare(coordenadas.x+2,coordenadas.y-1).setColor(Color.ORANGE);
				else if( control.getSquare(coordenadas.x+2,coordenadas.y-1).getPeca().getTeam()!=team){
					control.getSquare(coordenadas.x+2,coordenadas.y-1).setColor(Color.RED);	
					if(control.getSquare(coordenadas.x+2,coordenadas.y-1).getPeca().getName()=="Rei"){
						if(control.getSquare(coordenadas.x+2,coordenadas.y-1).getPeca().getTeam()==Color.WHITE)  
							JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
						else
							JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
					}
				}
			}
			//Pinta as casas possiveis a direita acima acima do cavalo com exceção
			if(coordenadas.x < 6 && coordenadas.y < 7){
				if(control.getSquare(coordenadas.x+2,coordenadas.y+1).getPeca()==null )
					control.getSquare(coordenadas.x+2,coordenadas.y+1).setColor(Color.ORANGE);
				else if( control.getSquare(coordenadas.x+2,coordenadas.y+1).getPeca().getTeam()!=team){
					control.getSquare(coordenadas.x+2,coordenadas.y+1).setColor(Color.RED);	
					if(control.getSquare(coordenadas.x+2,coordenadas.y+1).getPeca().getName()=="Rei"){
						if(control.getSquare(coordenadas.x+2,coordenadas.y+1).getPeca().getTeam()==Color.WHITE)  
							JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
						else
							JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
					}
				}
			}		
		}
    }
    