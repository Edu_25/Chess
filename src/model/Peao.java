package model;
import java.awt.Color;
import java.awt.Point;

import javax.swing.JOptionPane;

import control.SquareControl;
/*COMENTARIO A RESPEITO DA MOVIMENTAÇÃO:
 * A movimentação do peao se baseia em unicamente ir uma na vertical com execeção de seu primeiro movimento que pode dar dois na vertical
 * porém na hora de capturar peças ele captura somente na diagonal
 * Então foi necessario pintar as casas que estão que se distanciam duas na vertical para a primeira jogada da peça;
 * Uma para vertical nas seguintes jogadas
 * E pintar as casas uma na diagonal direita e outra na esquerda para indicar se a possibilidade de captura; 
 * Foi necessaria também medidas de contenção para que não houve-se erro na movimentação da peça;
*/
public class Peao extends Peca{
	public Peao(String name, String posicao, Color team){
		super(name,posicao,team);
	}
	
    @Override
    public void movimentaPeca(SquareControl control,Color team){
    
		Point coordenadas = control.getSelecionaCasa().getPosition();
		Square square = control.getSelecionaCasa();
		square.setColor(Color.ORANGE);
		//Pinta duas para cima as claras na primeira jogada
		if(team == Color.WHITE){
			if(coordenadas.x == 6){
				//Essa linha de baixo resolve o problema nos cantos 
				if(coordenadas.x > 0 && coordenadas.x < 7 && coordenadas.y >= 0 && coordenadas.y < 8){
					for(int contador = 1;contador < 3; contador++){
						if(control.getSquare(coordenadas.x - contador,coordenadas.y).getPeca() != null)
							break;//mas quero que isso so pare a seguinte linha 
						if(control.getSquare(coordenadas.x - contador,coordenadas.y).getPeca() == null)
							control.getSquare(coordenadas.x - contador,coordenadas.y).setColor(Color.ORANGE);		
					}
					//Comida
					if(coordenadas.x >= 0 && coordenadas.y > 0){
						if(control.getSquare(coordenadas.x - 1,coordenadas.y-1).getPeca()!= null){	
							if(control.getSquare(coordenadas.x-1,coordenadas.y-1).getPeca().getTeam()!=team){
								control.getSquare(coordenadas.x-1,coordenadas.y-1).setColor(Color.RED);
								if(control.getSquare(coordenadas.x-1,coordenadas.y-1).getPeca().getName()=="Rei"){
									if(control.getSquare(coordenadas.x-1,coordenadas.y-1).getPeca().getTeam()==Color.BLACK)  
										JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
								}
							}
						}
					}
					if(coordenadas.x > 0 && coordenadas.y < 7){
						if(control.getSquare(coordenadas.x - 1,coordenadas.y+1).getPeca()!= null){	
							if(control.getSquare(coordenadas.x-1,coordenadas.y+1).getPeca().getTeam()!=team){
								control.getSquare(coordenadas.x-1,coordenadas.y+1).setColor(Color.RED);
								if(control.getSquare(coordenadas.x-1,coordenadas.y+1).getPeca().getName()=="Rei"){
									if(control.getSquare(coordenadas.x-1,coordenadas.y+1).getPeca().getTeam()==Color.BLACK)  
										JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
								}
							}
						}
					}
				}
			}
			//Pinta uma para cima brancas
			else{
				if(coordenadas.x > 0 && coordenadas.x < 7 && coordenadas.y >= 0 && coordenadas.y < 8){
					if(control.getSquare(coordenadas.x-1,coordenadas.y).getPeca() == null)
						control.getSquare(coordenadas.x-1,coordenadas.y).setColor(Color.ORANGE);
			//Vê se tem possibilidade de captura
				if(coordenadas.x > 0 && coordenadas.y > 0){	
					if(control.getSquare(coordenadas.x - 1,coordenadas.y-1).getPeca()!= null){	
						if(control.getSquare(coordenadas.x-1,coordenadas.y-1).getPeca().getTeam()!=team){
							control.getSquare(coordenadas.x-1,coordenadas.y-1).setColor(Color.RED);
							if(control.getSquare(coordenadas.x-1,coordenadas.y-1).getPeca().getName()=="Rei"){
								if(control.getSquare(coordenadas.x-1,coordenadas.y-1).getPeca().getTeam()==Color.BLACK)  
									JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
							}
						}
					}
				}
					if(coordenadas.x > 0 && coordenadas.y < 7){	
					if(control.getSquare(coordenadas.x - 1,coordenadas.y+1).getPeca()!= null){	
						if(control.getSquare(coordenadas.x-1,coordenadas.y+1).getPeca().getTeam()!=team){
							control.getSquare(coordenadas.x-1,coordenadas.y+1).setColor(Color.RED);
							if(control.getSquare(coordenadas.x-1,coordenadas.y+1).getPeca().getName()=="Rei"){
								if(control.getSquare(coordenadas.x-1,coordenadas.y+1).getPeca().getTeam()==Color.BLACK)  
									JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
							}
						}
					}
				}
				}
			}
		}
		//Pinta duas para cima as escuras
		else{
			if(coordenadas.x == 1){
				if(coordenadas.x > 0 && coordenadas.x < 7 && coordenadas.y >= 0 && coordenadas.y < 8){
				for(int contador = 1;contador < 3; contador++){
					if(control.getSquare(coordenadas.x + contador,coordenadas.y).getPeca() != null)
							break;
					if(control.getSquare(coordenadas.x + contador,coordenadas.y).getPeca() == null)
						control.getSquare(coordenadas.x + contador,coordenadas.y).setColor(Color.ORANGE);		
				}
				//Comida
					if(coordenadas.x > 0 && coordenadas.y >= 0){
						if(control.getSquare(coordenadas.x + 1,coordenadas.y-1).getPeca()!= null){	
							if(control.getSquare(coordenadas.x+1,coordenadas.y-1).getPeca().getTeam()!=team){
								control.getSquare(coordenadas.x+1,coordenadas.y-1).setColor(Color.RED);
								if(control.getSquare(coordenadas.x+1,coordenadas.y-1).getPeca().getName()=="Rei"){
									if(control.getSquare(coordenadas.x+1,coordenadas.y-1).getPeca().getTeam()==Color.WHITE)  
										JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
								}
							}	
						}
					}
					if(coordenadas.x > 0 && coordenadas.y < 7){
						if(control.getSquare(coordenadas.x + 1,coordenadas.y+1).getPeca()!= null){	
							if(control.getSquare(coordenadas.x+1,coordenadas.y+1).getPeca().getTeam()!=team){
								control.getSquare(coordenadas.x+1,coordenadas.y+1).setColor(Color.RED);
								if(control.getSquare(coordenadas.x+1,coordenadas.y+1).getPeca().getName()=="Rei"){
									if(control.getSquare(coordenadas.x+1,coordenadas.y+1).getPeca().getTeam()==Color.WHITE)  
										JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
								}
							}
						}
					}
				}
			}
			//Pinta uma para cima
			else{
				if(coordenadas.x > 0 && coordenadas.x < 7 && coordenadas.y >= 0 && coordenadas.y < 8){
					if(control.getSquare(coordenadas.x+1,coordenadas.y).getPeca() == null)
						control.getSquare(coordenadas.x+1,coordenadas.y).setColor(Color.ORANGE);
			//Vê se tem possibilidade de captura para esquerda
				if(coordenadas.x < 7 && coordenadas.y >= 0){	
					if(control.getSquare(coordenadas.x + 1,coordenadas.y-1).getPeca()!= null){	
						if(control.getSquare(coordenadas.x+1,coordenadas.y-1).getPeca().getTeam()!=team){
							control.getSquare(coordenadas.x+1,coordenadas.y-1).setColor(Color.RED);
							if(control.getSquare(coordenadas.x+1,coordenadas.y-1).getPeca().getName()=="Rei"){
								if(control.getSquare(coordenadas.x+1,coordenadas.y-1).getPeca().getTeam()==Color.WHITE)  
									JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
							}
						}
					}
				}
				//Vê se tem possibilidade de captura para direita
				if(coordenadas.x < 7 && coordenadas.y < 7){	
					if(control.getSquare(coordenadas.x+1,coordenadas.y+1).getPeca()!= null){	
						if(control.getSquare(coordenadas.x+1,coordenadas.y+1).getPeca().getTeam()!=team){
							control.getSquare(coordenadas.x+1,coordenadas.y+1).setColor(Color.RED);
							if(control.getSquare(coordenadas.x+1,coordenadas.y+1).getPeca().getName()=="Rei"){
								if(control.getSquare(coordenadas.x+1,coordenadas.y+1).getPeca().getTeam()==Color.WHITE)  
									JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
							}
						}
					}
				}
			}
		}
		}
    }
    }
	