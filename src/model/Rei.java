package model;
import java.awt.Color;
import java.awt.Point;

import control.SquareControl;

/*COMENTARIO A RESPEITO DA MOVIMENTAÇÃO:
 * A movimentação da rei se baseia em uma casa para todos os lados partindo das coordenadas do rei no momento;
 * Então foi necessario pintar uma casa de todos os lados do rei, nesta tiveram que ser feita medidas de contenção para que não se 
 * tiver equivoco na movimentação da peça;
*/
public class Rei extends Peca{
	public Rei(String name, String posicao, Color team){
		super(name,posicao,team);
	}
    @Override
    public void movimentaPeca(SquareControl control,Color team){
    
		Point coordenadas = control.getSelecionaCasa().getPosition();

		//Pinta a casa diagonal esquerda acima do rei branco
		if(coordenadas.x>0 && coordenadas.y>0){
			if(control.getSquare(coordenadas.x-1,coordenadas.y-1).getPeca()==null)
				control.getSquare(coordenadas.x-1,coordenadas.y-1).setColor(Color.ORANGE);
			else if(control.getSquare(coordenadas.x-1,coordenadas.y-1).getPeca().getTeam()!=team){
				control.getSquare(coordenadas.x-1,coordenadas.y-1).setColor(Color.RED);
			}
		}
		//Pinta a casa diagonal esquerda abaixo do rei branco
		if(coordenadas.x<7 && coordenadas.y>0){
			if(control.getSquare(coordenadas.x+1,coordenadas.y-1).getPeca()==null)
				control.getSquare(coordenadas.x+1,coordenadas.y-1).setColor(Color.ORANGE);
				else if(control.getSquare(coordenadas.x+1,coordenadas.y-1).getPeca().getTeam()!=team){
				control.getSquare(coordenadas.x+1,coordenadas.y-1).setColor(Color.RED);
			}
		}
		//Pinta a casa diagonal direita acima do rei branco
			if(coordenadas.x>0 && coordenadas.y<7){
				if(control.getSquare(coordenadas.x-1,coordenadas.y+1).getPeca()==null)
					control.getSquare(coordenadas.x-1,coordenadas.y+1).setColor(Color.ORANGE);
				else if(control.getSquare(coordenadas.x-1,coordenadas.y+1).getPeca().getTeam()!=team){
					control.getSquare(coordenadas.x-1,coordenadas.y+1).setColor(Color.RED);
				}
			}
		//Pinta a casa diagonal direita abaixo do rei branco
			if(coordenadas.x < 7 && coordenadas.y < 7){
				if(control.getSquare(coordenadas.x+1,coordenadas.y+1).getPeca() == null)
					control.getSquare(coordenadas.x+1,coordenadas.y+1).setColor(Color.ORANGE);
				else if(control.getSquare(coordenadas.x+1,coordenadas.y+1).getPeca().getTeam()!=team){
					control.getSquare(coordenadas.x+1,coordenadas.y+1).setColor(Color.RED);
				}
			}
		//Pinta a casa acima do rei branco
		if(coordenadas.x>0){
			if(control.getSquare(coordenadas.x-1,coordenadas.y).getPeca()==null ){
				control.getSquare(coordenadas.x-1,coordenadas.y).setColor(Color.ORANGE);
			}
			else if(control.getSquare(coordenadas.x-1,coordenadas.y).getPeca().getTeam()!=team){
				control.getSquare(coordenadas.x-1,coordenadas.y).setColor(Color.RED);
			}
		}
		//Pinta a casa abaixo do rei escuro
		if(coordenadas.x<7){
			if(control.getSquare(coordenadas.x+1,coordenadas.y).getPeca() == null )
				control.getSquare(coordenadas.x+1,coordenadas.y).setColor(Color.ORANGE);
			else if(control.getSquare(coordenadas.x+1,coordenadas.y).getPeca().getTeam()!=team){
				control.getSquare(coordenadas.x+1,coordenadas.y).setColor(Color.RED);
			}		
		}
		//Pinta a casa esquerda do rei escuro
		if(coordenadas.y>0){
			if(control.getSquare(coordenadas.x,coordenadas.y-1).getPeca()==null)
				control.getSquare(coordenadas.x,coordenadas.y-1).setColor(Color.ORANGE);
			else if(control.getSquare(coordenadas.x,coordenadas.y-1).getPeca().getTeam()!=team){
				control.getSquare(coordenadas.x,coordenadas.y-1).setColor(Color.RED);
			}
		}
		//Pinta a casa direita do rei escuro
		if(coordenadas.y<7){
			if(control.getSquare(coordenadas.x,coordenadas.y+1).getPeca()==null){
				control.getSquare(coordenadas.x,coordenadas.y+1).setColor(Color.ORANGE);
			}
			else if(control.getSquare(coordenadas.x,coordenadas.y+1).getPeca().getTeam()!=team){
				control.getSquare(coordenadas.x,coordenadas.y+1).setColor(Color.RED);		
			}
		}
		//Implementando o roque
		/*if(control.roque){
		  	//Roque menor
		    if(coordenadas.y < 6){	
		    	if(control.getSquare(coordenadas.x,coordenadas.y+2).getPeca()==null){
		    		control.getSquare(coordenadas.x,coordenadas.y+2).setColor(Color.ORANGE);
		    		if(control.getSquare(coordenadas.x,coordenadas.y+3).getPeca()!=null){
		    			if(control.getSquare(coordenadas.x,coordenadas.y+3).getPeca().getTeam()==team){
		    				if(control.getSquare(coordenadas.x,coordenadas.y+3).getPeca().getName()=="Torre")
		    					control.getSquare(coordenadas.x,coordenadas.y+3).setColor(Color.GRAY);
		    			}
		    		}
		    	}
		   }	
	    }*/
    }
 }