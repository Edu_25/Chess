package model;

import java.awt.Color;
import java.awt.Point;

import javax.swing.JOptionPane;

import control.SquareControl;
import model.Peca;
/*COMENTARIO A RESPEITO DA MOVIMENTAÇÃO:
 * A movimentação da rainha se baseia em na junção da movimentação do bispo,da torre e do rei;
 * Então foi necessario pintar as casas que o rei,o bispo e a torre podem pintar de acordo com as coordenadas da rainha no momento;
*/
public class Rainha extends Peca {
	public Rainha(String name, String posicao, Color team){
		super(name,posicao,team);
}

@Override
public void movimentaPeca(SquareControl control,Color team){

	Point coordenadas = control.getSelecionaCasa().getPosition();
	//Movimento do Bispo
	//Pinta a diagonal inferior direita	
		for(int contador = 1; contador < 8; contador++) {					
			if(coordenadas.x + contador < 8 && coordenadas.y + contador < 8){
				if(control.getSquare(coordenadas.x + contador,coordenadas.y + contador).getPeca() == null){
					control.getSquare(coordenadas.x + contador,coordenadas.y+contador).setColor(Color.ORANGE);
				}
				else if(control.getSquare(coordenadas.x + contador,coordenadas.y + contador).getPeca().getTeam()!=team){
					control.getSquare(coordenadas.x + contador,coordenadas.y + contador).setColor(Color.RED);
					if(control.getSquare(coordenadas.x + contador,coordenadas.y + contador).getPeca().getName()=="Rei"){
						if(control.getSquare(coordenadas.x + contador,coordenadas.y + contador).getPeca().getTeam()==Color.WHITE)  
							JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
						else
							JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
					}
					break;
				}
				else
					break;
			}
		}
		//Pinta a diagonal inferior esquerda
			for(int contador = 1; contador < 8; contador++) {					
				if(coordenadas.x + contador < 8 && coordenadas.y - contador >= 0 ){
					if(control.getSquare(coordenadas.x + contador,coordenadas.y - contador).getPeca() == null){
						control.getSquare(coordenadas.x + contador,coordenadas.y - contador).setColor(Color.ORANGE);
					}
					else if(control.getSquare(coordenadas.x + contador,coordenadas.y - contador).getPeca().getTeam()!=team){
						control.getSquare(coordenadas.x + contador,coordenadas.y - contador).setColor(Color.RED);
						if(control.getSquare(coordenadas.x + contador,coordenadas.y - contador).getPeca().getName()=="Rei"){
							if(control.getSquare(coordenadas.x + contador,coordenadas.y - contador).getPeca().getTeam()==Color.WHITE)  
								JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
							else
								JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
						}
						break;
					}
					else
						break;
				}
			}
		//Pinta a diagonal superior esquerda
			for(int contador = 1; contador < 8; contador++) {					
				if(coordenadas.x - contador >= 0 && coordenadas.y - contador >= 0 ){
					if(control.getSquare(coordenadas.x - contador,coordenadas.y - contador).getPeca() == null){
						control.getSquare(coordenadas.x - contador,coordenadas.y - contador).setColor(Color.ORANGE);
					}
					else if(control.getSquare(coordenadas.x - contador,coordenadas.y - contador).getPeca().getTeam()!=team){
						control.getSquare(coordenadas.x - contador,coordenadas.y - contador).setColor(Color.RED);
						if(control.getSquare(coordenadas.x - contador,coordenadas.y - contador).getPeca().getName()=="Rei"){
							if(control.getSquare(coordenadas.x - contador,coordenadas.y - contador).getPeca().getTeam()==Color.WHITE)  
								JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
							else
								JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
						}
						break;
					}
					else
						break;
				}
			}
			
	//Pinta a diagonal superior direita
		for(int contador = 1; contador < 8; contador++) {					
			if(coordenadas.x - contador >= 0 && coordenadas.y + contador < 8){
				if(control.getSquare(coordenadas.x - contador,coordenadas.y + contador).getPeca() == null){
					control.getSquare(coordenadas.x - contador,coordenadas.y + contador).setColor(Color.ORANGE);
				}
				else if(control.getSquare(coordenadas.x - contador,coordenadas.y + contador).getPeca().getTeam()!=team){
					control.getSquare(coordenadas.x - contador,coordenadas.y + contador).setColor(Color.RED);
					if(control.getSquare(coordenadas.x - contador,coordenadas.y + contador).getPeca().getName()=="Rei"){
						if(control.getSquare(coordenadas.x - contador,coordenadas.y + contador).getPeca().getTeam()==Color.WHITE)  
							JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
						else
							JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
					}
					break;
				}
				else
					break;
				}
		}
		
	//Movimenta da Torre
		//Pinta as casas possiveis a direita da torre
				for(int contador = coordenadas.y+1; contador < SquareControl.COL_NUMBER; contador++){
							if(control.getSquare(coordenadas.x,contador).getPeca() == null){
								control.getSquare(coordenadas.x,contador).setColor(Color.ORANGE);
							}
							else if(control.getSquare(coordenadas.x,contador).getPeca().getTeam()!=team){
								control.getSquare(coordenadas.x,contador).setColor(Color.RED);
								if(control.getSquare(coordenadas.x,contador).getPeca().getName()=="Rei"){
									if(control.getSquare(coordenadas.x,contador).getPeca().getTeam()==Color.WHITE)  
										JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
									else
										JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
								}
								break;
							}
							else
								break;
				}
				//Pinta as casas possiveis a esquerda da torre
				for(int contador = coordenadas.y-1; contador >= SquareControl.DOWN_NUMBER; contador--){
						if(control.getSquare(coordenadas.x,contador).getPeca() == null)
								control.getSquare(coordenadas.x,contador).setColor(Color.ORANGE);
						
						else if(control.getSquare(coordenadas.x,contador).getPeca().getTeam()!=team){
							control.getSquare(coordenadas.x,contador).setColor(Color.RED);
							if(control.getSquare(coordenadas.x,contador).getPeca().getName()=="Rei"){
								if(control.getSquare(coordenadas.x,contador).getPeca().getTeam()==Color.WHITE)  
									JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
								else
									JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
							}
							break;
						}
						else
								break;		
				}
				//Pinta as casas possiveis a baixo da torre
						for(int contador = coordenadas.x+1; contador < SquareControl.COL_NUMBER; contador++){
							if(control.getSquare(contador,coordenadas.y).getPeca() == null)
								control.getSquare(contador,coordenadas.y).setColor(Color.ORANGE);
							else if(control.getSquare(contador,coordenadas.y).getPeca().getTeam()!=team){
								control.getSquare(contador,coordenadas.y).setColor(Color.RED);
								if(control.getSquare(contador,coordenadas.y).getPeca().getName()=="Rei"){
									if(control.getSquare(contador,coordenadas.y).getPeca().getTeam()==Color.WHITE)  
										JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
									else
										JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
								}
								break;
							}
							else
								break;
							
						}
				//Pinta as casas possiveis a cima da torre			
				for(int contador = coordenadas.x-1; contador >= SquareControl.DOWN_NUMBER; contador--){
				if(control.getSquare(contador,coordenadas.y).getPeca() == null){
						control.getSquare(contador,coordenadas.y).setColor(Color.ORANGE);
				}else if(control.getSquare(contador,coordenadas.y).getPeca().getTeam()!=team){
					control.getSquare(contador,coordenadas.y).setColor(Color.RED);
					if(control.getSquare(contador,coordenadas.y).getPeca().getName()=="Rei"){
						if(control.getSquare(contador,coordenadas.y).getPeca().getTeam()==Color.WHITE)  
							JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
						else
							JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
					}
					break;
				}
				else
						break;
				}

		//Movimentos do rei
				//Pinta a casa diagonal esquerda acima do rei branco
				if(coordenadas.x>0 && coordenadas.y>0){
					if(control.getSquare(coordenadas.x-1,coordenadas.y-1).getPeca()==null)
						control.getSquare(coordenadas.x-1,coordenadas.y-1).setColor(Color.ORANGE);
					else if(control.getSquare(coordenadas.x-1,coordenadas.y-1).getPeca().getTeam()!=team){
						control.getSquare(coordenadas.x-1,coordenadas.y-1).setColor(Color.RED);
						if(control.getSquare(coordenadas.x-1,coordenadas.y-1).getPeca().getName()=="Rei"){
							if(control.getSquare(coordenadas.x-1,coordenadas.y-1).getPeca().getTeam()==Color.WHITE)  
								JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
							else
								JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
						}
					}
				}
				//Pinta a casa diagonal esquerda abaixo do rei branco
				if(coordenadas.x<7 && coordenadas.y>0){
					if(control.getSquare(coordenadas.x+1,coordenadas.y-1).getPeca()==null)
						control.getSquare(coordenadas.x+1,coordenadas.y-1).setColor(Color.ORANGE);
						else if(control.getSquare(coordenadas.x+1,coordenadas.y-1).getPeca().getTeam()!=team){
						control.getSquare(coordenadas.x+1,coordenadas.y-1).setColor(Color.RED);
						if(control.getSquare(coordenadas.x+1,coordenadas.y-1).getPeca().getName()=="Rei"){
							if(control.getSquare(coordenadas.x+1,coordenadas.y-1).getPeca().getTeam()==Color.WHITE)  
								JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
							else
								JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
						}
					}
				}
				//Pinta a casa diagonal direita acima do rei branco
					if(coordenadas.x>0 && coordenadas.y<7){
						if(control.getSquare(coordenadas.x-1,coordenadas.y+1).getPeca()==null)
							control.getSquare(coordenadas.x-1,coordenadas.y+1).setColor(Color.ORANGE);
						else if(control.getSquare(coordenadas.x-1,coordenadas.y+1).getPeca().getTeam()!=team){
							control.getSquare(coordenadas.x-1,coordenadas.y+1).setColor(Color.RED);
							if(control.getSquare(coordenadas.x-1,coordenadas.y+1).getPeca().getName()=="Rei"){
								if(control.getSquare(coordenadas.x-1,coordenadas.y+1).getPeca().getTeam()==Color.WHITE)  
									JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
								else
									JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
							}
						}
					}
				//Pinta a casa diagonal direita abaixo do rei branco
					if(coordenadas.x < 7 && coordenadas.y < 7){
						if(control.getSquare(coordenadas.x+1,coordenadas.y+1).getPeca() == null)
							control.getSquare(coordenadas.x+1,coordenadas.y+1).setColor(Color.ORANGE);
						else if(control.getSquare(coordenadas.x+1,coordenadas.y+1).getPeca().getTeam()!=team){
							control.getSquare(coordenadas.x+1,coordenadas.y+1).setColor(Color.RED);
							if(control.getSquare(coordenadas.x+1,coordenadas.y+1).getPeca().getName()=="Rei"){
								if(control.getSquare(coordenadas.x+1,coordenadas.y+1).getPeca().getTeam()==Color.WHITE)  
									JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
								else
									JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
							}
						}
					}
				//Pinta a casa acima do rei branco
				if(coordenadas.x>0){
					if(control.getSquare(coordenadas.x-1,coordenadas.y).getPeca()==null ){
						control.getSquare(coordenadas.x-1,coordenadas.y).setColor(Color.ORANGE);
					}
					else if(control.getSquare(coordenadas.x-1,coordenadas.y).getPeca().getTeam()!=team){
						control.getSquare(coordenadas.x-1,coordenadas.y).setColor(Color.RED);
						if(control.getSquare(coordenadas.x-1,coordenadas.y).getPeca().getName()=="Rei"){
							if(control.getSquare(coordenadas.x-1,coordenadas.y).getPeca().getTeam()==Color.WHITE)  
								JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
							else
								JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
						}
					}
				}
				//Pinta a casa abaixo do rei branco
				if(coordenadas.x<7){
					if(control.getSquare(coordenadas.x+1,coordenadas.y).getPeca() == null )
						control.getSquare(coordenadas.x+1,coordenadas.y).setColor(Color.ORANGE);
					else if(control.getSquare(coordenadas.x+1,coordenadas.y).getPeca().getTeam()!=team){
						control.getSquare(coordenadas.x+1,coordenadas.y).setColor(Color.RED);
						if(control.getSquare(coordenadas.x+1,coordenadas.y).getPeca().getName()=="Rei"){
							if(control.getSquare(coordenadas.x+1,coordenadas.y).getPeca().getTeam()==Color.WHITE)  
								JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
							else
								JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
						}
					}		
				}
				//Pinta a casa esquerda do rei branco
				if(coordenadas.y>0){
					if(control.getSquare(coordenadas.x,coordenadas.y-1).getPeca()==null)
						control.getSquare(coordenadas.x,coordenadas.y-1).setColor(Color.ORANGE);
					else if(control.getSquare(coordenadas.x,coordenadas.y-1).getPeca().getTeam()!=team){
						control.getSquare(coordenadas.x,coordenadas.y-1).setColor(Color.RED);
						if(control.getSquare(coordenadas.x,coordenadas.y-1).getPeca().getName()=="Rei"){
							if(control.getSquare(coordenadas.x,coordenadas.y-1).getPeca().getTeam()==Color.WHITE)  
								JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
							else
								JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
						}
					}
				}
				//Pinta a casa direita do rei branco
				if(coordenadas.y<7){
					if(control.getSquare(coordenadas.x,coordenadas.y+1).getPeca()==null){
						control.getSquare(coordenadas.x,coordenadas.y+1).setColor(Color.ORANGE);
					}
					else if(control.getSquare(coordenadas.x,coordenadas.y+1).getPeca().getTeam()!=team){
						control.getSquare(coordenadas.x,coordenadas.y+1).setColor(Color.RED);
						if(control.getSquare(coordenadas.x,coordenadas.y+1).getPeca().getName()=="Rei"){
							if(control.getSquare(coordenadas.x,coordenadas.y+1).getPeca().getTeam()==Color.WHITE)  
								JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
							else
								JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
							}
						}		
			}
	}
}
