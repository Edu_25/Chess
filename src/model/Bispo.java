package model;

import java.awt.Color;
import java.awt.Point;

import javax.swing.JOptionPane;

import control.SquareControl;
import model.Peca;
/*COMENTARIO A RESPEITO DA MOVIMENTAÇÃO:
 * A movimentação do bispo se baseia em diagonais
 * Então foi necessario pintar as casas que estão que se distanciam uma na horizontal e uma na vertical para as quatro direções 
 * partindo-se das coordenadas do bispo no momento;
*/
public class Bispo extends Peca {
	public Bispo(String name, String posicao, Color team){
		super(name,posicao,team);
	}

@Override
public void movimentaPeca(SquareControl control,Color team){

	Point coordenadas = control.getSelecionaCasa().getPosition();
	//Square square = control.getSelecionaCasa();
//	square.setColor(Color.ORANGE);
//Pinta a diagonal inferior direita	
	for(int contador = 1; contador < 8; contador++) {					
		if(coordenadas.x + contador < 8 && coordenadas.y + contador < 8){
			if(control.getSquare(coordenadas.x + contador,coordenadas.y + contador).getPeca() == null){
				control.getSquare(coordenadas.x + contador,coordenadas.y+contador).setColor(Color.ORANGE);
			}
			else if(control.getSquare(coordenadas.x + contador,coordenadas.y + contador).getPeca().getTeam()!=team){
				control.getSquare(coordenadas.x + contador,coordenadas.y + contador).setColor(Color.RED);
				if(control.getSquare(coordenadas.x + contador,coordenadas.y + contador).getPeca().getName()=="Rei"){
					if(control.getSquare(coordenadas.x + contador,coordenadas.y + contador).getPeca().getTeam()==Color.WHITE)  
						JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
					else
						JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
				}
				break;
			}
			else
				break;
		}
	}
	//Pinta a diagonal inferior esquerda
		for(int contador = 1; contador < 8; contador++) {					
			if(coordenadas.x + contador < 8 && coordenadas.y - contador >= 0 ){
				if(control.getSquare(coordenadas.x + contador,coordenadas.y - contador).getPeca() == null){
					control.getSquare(coordenadas.x + contador,coordenadas.y - contador).setColor(Color.ORANGE);
				}
				else if(control.getSquare(coordenadas.x + contador,coordenadas.y - contador).getPeca().getTeam()!=team){
					control.getSquare(coordenadas.x + contador,coordenadas.y - contador).setColor(Color.RED);
					if(control.getSquare(coordenadas.x + contador,coordenadas.y - contador).getPeca().getName()=="Rei"){
						if(control.getSquare(coordenadas.x + contador,coordenadas.y - contador).getPeca().getTeam()==Color.WHITE)  
							JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
						else
							JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
					}
					break;
				}
				else
					break;
			}
		}
	//Pinta a diagonal superior esquerda
		for(int contador = 1; contador < 8; contador++) {					
			if(coordenadas.x - contador >= 0 && coordenadas.y - contador >= 0 ){
				if(control.getSquare(coordenadas.x - contador,coordenadas.y - contador).getPeca() == null){
					control.getSquare(coordenadas.x - contador,coordenadas.y - contador).setColor(Color.ORANGE);
				}
				else if(control.getSquare(coordenadas.x - contador,coordenadas.y - contador).getPeca().getTeam()!=team){
					control.getSquare(coordenadas.x - contador,coordenadas.y - contador).setColor(Color.RED);
					if(control.getSquare(coordenadas.x - contador,coordenadas.y - contador).getPeca().getName()=="Rei"){
						if(control.getSquare(coordenadas.x - contador,coordenadas.y - contador).getPeca().getTeam()==Color.WHITE)  
							JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
						else
							JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
					}
					break;
				}
				else
					break;
			}
			else{
				
			}
		}
//Pinta a diagonal superior direita
	for(int contador = 1; contador < 8; contador++) {					
		if(coordenadas.x - contador >= 0 && coordenadas.y + contador < 8){
			if(control.getSquare(coordenadas.x - contador,coordenadas.y + contador).getPeca() == null){
				control.getSquare(coordenadas.x - contador,coordenadas.y + contador).setColor(Color.ORANGE);
			}
			else if(control.getSquare(coordenadas.x - contador,coordenadas.y + contador).getPeca().getTeam()!=team){
				control.getSquare(coordenadas.x - contador,coordenadas.y + contador).setColor(Color.RED);
				if(control.getSquare(coordenadas.x - contador,coordenadas.y + contador).getPeca().getName()=="Rei"){
					if(control.getSquare(coordenadas.x - contador,coordenadas.y + contador).getPeca().getTeam()==Color.WHITE)  
						JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
					else
						JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
				}
				break;
			}
			else
				break;
			}
	}
	}
}