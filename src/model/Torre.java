package model;
import java.awt.Color;
import java.awt.Point;

import javax.swing.JOptionPane;

import control.SquareControl;
/*COMENTARIO A RESPEITO DA MOVIMENTAÇÃO:
 * A movimentação da torre se baseia em linhas perpendiculares
 * Então foi necessario pintar as casas que estão na horizontal e vertical partindo-se das coordenadas da torre no momento;
*/
public class Torre extends Peca{
	public Torre(String name, String posicao, Color team){
		super(name,posicao,team);
	}

    @Override
    public void movimentaPeca(SquareControl control,Color team){
    
		Point coordenadas = control.getSelecionaCasa().getPosition();
		//Square square = control.getSelecionaCasa();
		//square.setColor(Color.ORANGE);
		//Pinta as casas possiveis a direita da torre
		for(int contador = coordenadas.y+1; contador < SquareControl.COL_NUMBER; contador++){
					if(control.getSquare(coordenadas.x,contador).getPeca() == null){
						control.getSquare(coordenadas.x,contador).setColor(Color.ORANGE);
					}
					else if(control.getSquare(coordenadas.x,contador).getPeca().getTeam()!=team){
						control.getSquare(coordenadas.x,contador).setColor(Color.RED);
						if(control.getSquare(coordenadas.x,contador).getPeca().getName()=="Rei"){
							if(control.getSquare(coordenadas.x,contador).getPeca().getTeam()==Color.WHITE)  
								JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
							else
								JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
						}
						break;
					}
					else
						break;
		}
		//Pinta as casas possiveis a esquerda da torre
		for(int contador = coordenadas.y-1; contador >= SquareControl.DOWN_NUMBER; contador--){
				if(control.getSquare(coordenadas.x,contador).getPeca() == null)
						control.getSquare(coordenadas.x,contador).setColor(Color.ORANGE);
				
				else if(control.getSquare(coordenadas.x,contador).getPeca().getTeam()!=team){
					control.getSquare(coordenadas.x,contador).setColor(Color.RED);
					if(control.getSquare(coordenadas.x,contador).getPeca().getName()=="Rei"){
						if(control.getSquare(coordenadas.x,contador).getPeca().getTeam()==Color.WHITE)  
							JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
						else
							JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
					}
					break;
				}
				else
						break;		
		}
		//Pinta as casas possiveis a baixo da torre
				for(int contador = coordenadas.x+1; contador < SquareControl.COL_NUMBER; contador++){
					if(control.getSquare(contador,coordenadas.y).getPeca() == null)
						control.getSquare(contador,coordenadas.y).setColor(Color.ORANGE);
					else if(control.getSquare(contador,coordenadas.y).getPeca().getTeam()!=team){
						control.getSquare(contador,coordenadas.y).setColor(Color.RED);
						if(control.getSquare(contador,coordenadas.y).getPeca().getName()=="Rei"){
							if(control.getSquare(contador,coordenadas.y).getPeca().getTeam()==Color.WHITE)  
								JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
							else
								JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
						}
						break;
					}
					else
						break;
					
				}
		//Pinta as casas possiveis a cima da torre			
		for(int contador = coordenadas.x-1; contador >= SquareControl.DOWN_NUMBER; contador--){
		if(control.getSquare(contador,coordenadas.y).getPeca() == null){
				control.getSquare(contador,coordenadas.y).setColor(Color.ORANGE);
		}else if(control.getSquare(contador,coordenadas.y).getPeca().getTeam()!=team){
			control.getSquare(contador,coordenadas.y).setColor(Color.RED);
			if(control.getSquare(contador,coordenadas.y).getPeca().getName()=="Rei"){
				if(control.getSquare(contador,coordenadas.y).getPeca().getTeam()==Color.WHITE)  
					JOptionPane.showMessageDialog(null,"Xeque no Rei claro");
				else
					JOptionPane.showMessageDialog(null,"Xeque no Rei escuro");
			}
			break;
		}
		else
				break;
		}
    }
}