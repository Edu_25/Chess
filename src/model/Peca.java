package model;
import java.awt.Color;

import control.SquareControl;
public abstract class Peca {
	
	//Atributos da classe Peca
	private String name;
			String posicao;
			Color team;
			
	public Peca(String name, String posicao, Color team){
		this.posicao = posicao;
		this.name = name;
		this.team = team;
	}
	//Método de movimento da peça
		public abstract void movimentaPeca(SquareControl control, Color team);
	//Métodos 
		//Gets
	public String getPosicao() {
		return posicao;
	}
	public String getName() {
		return name;
	}
	public Color getTeam() {
		return team;
	}
		//Sets
	public void setPosition(String posicao) {
		this.posicao = posicao;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setTeam(Color team) {
		this.team = team;
	}
}
