package control;

import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Peca;
import model.Rei;
import model.Square;
import model.Square.SquareEventListener;
public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;
	//Criei para auxiliar numa possivel modificação
	public static final int DOWN_NUMBER = 0;
	public static final int UPPER_NUMBER = 64;
	
	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.BLACK;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.ORANGE;
	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	//private Color colorSelected;
	private boolean naoResetar;
	//roque
	public boolean roque = true;
	private Square selectedSquare;
	private ArrayList<Square> squareList;

	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,DEFAULT_COLOR_SELECTED);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,Color colorSelected) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		//this.colorSelected = colorSelected;

		this.squareList = new ArrayList<>();
		createSquares();
	}
	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;
			square.setColor(getGridColor(row, col));
	}
	//Aplica o hover
	@Override
	public void onHoverEvent(Square square) {
	//	System.out.println(square.getPosition());
		if(square.getColor() == Color.RED || square.getColor() == Color.ORANGE ){
			reset(true);
			if(square.getColor() == Color.ORANGE)
				square.setColor(Color.ORANGE);
			if(square.getColor() == Color.RED)
				square.setColor(Color.RED);
		}
		else {
			reset(false);
			square.setColor(this.colorHover);
		}
	}
	public void reset(boolean aux){
		if(aux == true)
			naoResetar = true;
		else
			naoResetar = false;
	}
//QUando selecionar a peça fazer com que só movimente para a area pintada
	@Override
	public void onSelectEvent(Square square) {
		if (haveSelectedCellPanel()) {
			if ((!this.selectedSquare.equals(square) && square.getColor() == Color.ORANGE) || (!this.selectedSquare.equals(square) && square.getColor() == Color.RED)) {
				moveContentOfSelectedSquare(square);
				//numeroJogada++;
				ResetarCor(squareList);
			}
			else if(this.selectedSquare.equals(square)){
				unselectSquare(square);
				ResetarCor(squareList);	
			}
			else{
				unselectSquare(square);
				ResetarCor(squareList);
				JOptionPane.showMessageDialog(null,"Movimento inválido! Por favor, movimentar a peça somente na área pintada");
			}
		} 
		else {
			selectSquare(square);
		}
	}

	@Override
	public void onOutEvent(Square square) {
		if (this.selectedSquare != square && naoResetar != true) {
			resetColor(square);
		} 
	}

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}
	//Pinta o tabuleiro
	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} 
		else {
			return this.colorTwo;
		}
	}
	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;
		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}
	private void moveContentOfSelectedSquare(Square square) {
		//Para o jogo caso o rei seja capturado
		if(square.havePeca()){
			if(square.getPeca().getClass()==Rei.class){
				JOptionPane.showMessageDialog(null, "Fatality!!!");
				System.exit(0);
			}		
		}
		
		square.setPeca(this.selectedSquare.getPeca());
		//Implementando o roque
		/*if(roque){
			//Roque menor
			if(this.selectedSquare.getPosition().x==7 && this.selectedSquare.getPosition().y==4 && this.selectedSquare.getPeca().getClass()==Rei.class){
				getSquare(7,5).setPeca(getSquare(7,7).getPeca());
				getSquare(7,7).removePeca();
				roque = false;
			}
			//Roque maior
			if(this.selectedSquare.getPosition().x==7 && this.selectedSquare.getPosition().y==4 && this.selectedSquare.getPeca().getClass()==Rei.class){
				getSquare(7,0).setPeca(getSquare(7,3).getPeca());
				getSquare(7,0).removePeca();
				roque = false;
			}
		}*/
		this.selectedSquare.removePeca();
		unselectSquare(square);
	}
	//Seleciona o quadrado que contém o nome,imagem e cor
	private void selectSquare(Square square) {
		if (square.havePeca()) {
			Color team = square.getPeca().getTeam();
			//Avaliando qual é a peça e qual é a cor dela e depois chama o método 

			Peca peca= square.getPeca();
				if(team == Color.WHITE){
					this.selectedSquare = square;
					peca.movimentaPeca(this,Color.WHITE);
				}
				else{
					this.selectedSquare = square;
					peca.movimentaPeca(this,Color.BLACK);
				}
		}
	}
	
	private void unselectSquare(Square square) {
		resetColor(this.selectedSquare);
		this.selectedSquare = EMPTY_SQUARE;
	}
	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}
	//Método criado para selecionar as casas 
	public Square getSelecionaCasa() {
		return selectedSquare;
	}
	
	public void setSelecionaCasa(Square square){
		this.selectedSquare = square;
	}
	//Método criado para resetar as casas 
	private void ResetarCor(ArrayList<Square> squaresList) {
		Square square;
		for(int contador = DOWN_NUMBER; contador < UPPER_NUMBER; contador++){
			square = squareList.get(contador);
			resetColor(square);
		}
	}
}
