package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import model.Square;
import control.SquareControl;

import model.Bispo;
import model.Cavalo;
import model.Peao;
import model.Peca;
import model.Rainha;
import model.Rei;
import model.Torre;


public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.BLACK;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.ORANGE;
		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,colorSelected);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;
			SquarePanel squarePanel = new SquarePanel(square);
			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}
	}
//Inicilaliza as peças do xadrez
	private void initializePiecesInChess() {
		
		//Peças Claras		
		Peca peao_claro = new Peao("Peao","icon/White P_48x48.png",Color.WHITE);
		for (int i = 0; i < SquareControl.COL_NUMBER; i++)
			this.squareControl.getSquare(6, i).setPeca(peao_claro);
		
		Peca torre_claro = new Torre("Torre","icon/White R_48x48.png",Color.WHITE);	
		this.squareControl.getSquare(7, 0).setPeca(torre_claro);
		this.squareControl.getSquare(7, 7).setPeca(torre_claro);
		
		
		Peca bispo_claro = new Bispo("Bispo","icon/White B_48x48.png",Color.WHITE);
		this.squareControl.getSquare(7, 2).setPeca(bispo_claro);
		this.squareControl.getSquare(7, 5).setPeca(bispo_claro);
		
 		Peca cavalo_claro = new Cavalo("Cavalo","icon/White N_48x48.png",Color.WHITE);	
		this.squareControl.getSquare(7, 1).setPeca(cavalo_claro);
		this.squareControl.getSquare(7, 6).setPeca(cavalo_claro);
		
		Peca rainha_claro = new Rainha("Rainha","icon/White Q_48x48.png",Color.WHITE);	
		this.squareControl.getSquare(7, 3).setPeca(rainha_claro);
		
		Peca rei_claro = new Rei("Rei","icon/White K_48x48.png",Color.WHITE);
		this.squareControl.getSquare(7, 4).setPeca(rei_claro);
		
		//Peças Escuras
		Peca peao = new Peao("Peao","icon/Black P_48x48.png",Color.BLACK);
		for(int i = 0; i < SquareControl.COL_NUMBER; i++)
		this.squareControl.getSquare(1, i).setPeca(peao);
			
		Peca torre_escura = new Torre("Torre","icon/Black R_48x48.png",Color.BLACK);
		this.squareControl.getSquare(0, 0).setPeca(torre_escura);
		this.squareControl.getSquare(0, 7).setPeca(torre_escura);
				
		Peca bispo_escura = new Bispo("Bispo","icon/Black B_48x48.png",Color.BLACK);
		this.squareControl.getSquare(0, 2).setPeca(bispo_escura);
		this.squareControl.getSquare(0, 5).setPeca(bispo_escura);		
				
		Peca cavalo_escura = new Cavalo("Cavalo","icon/Black N_48x48.png",Color.BLACK);
		this.squareControl.getSquare(0, 1).setPeca(cavalo_escura);
		this.squareControl.getSquare(0, 6).setPeca(cavalo_escura);
				
		Peca rainha_escura = new Rainha("Rainha","icon/Black Q_48x48.png",Color.BLACK);
		this.squareControl.getSquare(0, 3).setPeca(rainha_escura);
				
		Peca rei_escura = new Rei("Rei","icon/Black K_48x48.png",Color.BLACK);
		this.squareControl.getSquare(0, 4).setPeca(rei_escura);
	}
}
